from bs4 import BeautifulSoup, UnicodeDammit
from urllib import request
import os

current_path = "/home/hyperion/Arduino/semaine.edt"

output = ""

def __standard(debut, fin):
    i = 0
    caracteres = list(debut)
    while debut[i] != 'h':
        i += 1
    caracteres[i] = ":"
    if i == len(debut) - 1:
        caracteres += "0"
        caracteres += "0"
    debut = "".join(caracteres)
    i = 0
    while fin[i] != 'h':
        i += 1
    caracteres = list(fin)
    caracteres[i] = ":"
    if i == len(fin) - 1:
        caracteres += "0"
        caracteres += "0"
    fin = "".join(caracteres)
    return(debut, fin)

def __intra_heure(matiere):
    global output
    brut_heure = str(matiere)
    i = 1
    while ord(brut_heure[i]) < 48 or ord(brut_heure[i]) > 57:
        if ord(brut_heure[i]) == '<':
            i = -1
            break
        i += 1
    if i > 0:
        #if (matiere.find('p').string)[0] != 'Q':
        heure = ""
        for j in range(13):
            heure += brut_heure[i + j]
        debut, fin = heure.split(" - ", 1)
        #Ajout dans la sortie
        output += debut
        output += '\n'
        #output += fin
        #output += '\n'
    return 1
    #else:
    #    return 0

def __intra_QCM(matiere):
    global output
    accu_matiere = matiere.find('p').string
    if accu_matiere[0] == 'Q':
        print(accu_matiere)
        new_matiere, rien, rerien, horaire, rienbis = accu_matiere.split(' ', 4)
        debut, fin = horaire.split('/', 1)
        #11h15, 13h -> 11:15, 13:00
        debut, fin = __standard(debut, fin)
        output += debut
        output += '\n'
        output += fin
        output += '\n'
        output += new_matiere
        output += '\n'
        list_class = ""
        for classe in matiere.find_all('a'):
            list_class += "".join((classe.string).split(" ", 1))
            list_class += " "
        list_class = list_class[:-1]
        output += list_class
        output += '\n'

def __intra_matiere(day):
    global output
    for matiere in day.find_all('li'):
        if __intra_heure(matiere):
            #Gestion du cas QCM
            #if (matiere.find('p').string)[0] == 'Q':
            #    __intra_QCM(matiere)
            #else:
            accu_matiere = matiere.find('p').string
            output += accu_matiere
            output += '\n'
            accu_salle = matiere.find('a').string
            output += accu_salle
            output += '\n'

def __intra_day(day):
    global output
    today = day.find('h3').string
    if today[0] == 'A':
        rien, today = (today.string).split(" : ", 1)
    #Type Tag->str
    jour, date, mois = today.split(" ", 2)
    #Ajout dans la sortie
    #output += jour
    #output += '\n'
    output += date
    output += '\n'
    #output += mois
    #output += '\n'

#Jour par jour
def __day(brut):
    for div in brut.find_all('div'):
        for i in div.get("class"):
            if i == "day":
                #Debut extraction
                __intra_day(div)
                __intra_matiere(div)

def main():
    global output
    #Ouverture de l'url
    res_html = request.urlopen("https://ichronos.net/ING1%2FA1%2FPT/")
    #Lecture du fichier html
    html = res_html.read()
    #Reencodage en Unicode puis ASCII (str->str)
    txt_html = UnicodeDammit(html).unicode_markup.encode('ascii', 'ignore')
    brut = BeautifulSoup(txt_html)
    __day(brut)
    #print(output)
    output = output[:-1]
    if os.path.exists(current_path):
        os.remove(current_path)
    file = open("semaine.edt", "w")
    file.write(output)
    file.close()
