from flask import Flask
import parse
application = Flask(__name__)

@application.route("/")
def hello():
    return "<h1 style='color:blue'>Hello There!</h1>"

@application.route("/run")
def execute():
    parse.main()
    return "<h1>Execution du programme de parse HTML</h1>"

@application.route("/edt")
def edt():
    with open('semaine.edt', 'r') as txt:
        return txt.read()
    txt.close()
